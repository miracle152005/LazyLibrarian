#  This file is part of Lazylibrarian.
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.


import os
import string
import traceback
import subprocess
import json
import io
import zipfile
import logging
import tempfile

import lazylibrarian
from lazylibrarian.config2 import CONFIG
from lazylibrarian import database
from lazylibrarian.bookwork import get_bookwork, NEW_WHATWORK
from lazylibrarian.formatter import plural, make_unicode, make_bytestr, safe_unicode, check_int, make_utf8bytes
from lazylibrarian.filesystem import DIRS, path_isfile, syspath, setperm, safe_copy, jpg_file
from lazylibrarian.cache import cache_img, fetch_url, ImageType
from lazylibrarian.blockhandler import BLOCKHANDLER
from urllib.parse import quote_plus
from shutil import rmtree
from secrets import choice

try:
    import PIL
except ImportError:
    PIL = None
if PIL:
    # noinspection PyUnresolvedReferences
    from PIL import Image as PILImage
    from lib.icrawler.builtin import GoogleImageCrawler, BingImageCrawler, BaiduImageCrawler, FlickrImageCrawler
else:
    GoogleImageCrawler = None
    BingImageCrawler = None
    BaiduImageCrawler = None
    FlickrImageCrawler = None

# noinspection PyProtectedMember
from pypdf import PdfWriter, PdfReader

# noinspection PyBroadException
try:
    import magic
except Exception:  # magic might fail for multiple reasons
    magic = None

GS = ''
GS_VER = ''
generator = ''


def img_id(length=10):
    return ''.join([choice(string.ascii_letters + string.digits) for _ in range(length)])


def createthumbs(jpeg):
    if not PIL:
        return
    for basewidth in (100, 200, 300, 500):
        createthumb(jpeg, basewidth, overwrite=False)


def createthumb(jpeg, basewidth=None, overwrite=True):
    if not PIL:
        return ''
    logger = logging.getLogger(__name__)
    fname, extn = os.path.splitext(jpeg)
    outfile = f"{fname}_w{basewidth}{extn}" if basewidth else f"{fname}_thumb{extn}"

    if not overwrite and path_isfile(outfile):
        return outfile

    if not path_isfile(jpeg):
        logger.debug(f"Cannot open {jpeg} for thumbnail")
        return ''

    bwidth = basewidth if basewidth else 300
    try:
        img = PILImage.open(jpeg)
    except Exception as e:
        logger.debug(str(e))
        if magic:
            try:
                mtype = magic.from_file(jpeg).upper()
                logger.debug(f"magic reports {mtype}")
            except Exception as e:
                logger.debug(f"{type(e).__name__} reading magic from {jpeg}, {e}")
        return ''

    wpercent = (bwidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    try:
        # noinspection PyUnresolvedReferences
        img = img.resize((bwidth, hsize), PIL.Image.LANCZOS)
        img.save(outfile)
    except OSError:
        try:
            img.convert('RGB').save(outfile)
        except OSError:
            return ''
    logger.debug(f"Created {outfile}")
    setperm(outfile)
    return outfile


def valid_pdf(sourcefile):
    # check we can read/parse the pdf file, ie it's not corrupted
    logger = logging.getLogger(__name__)
    if PdfWriter is None:
        logger.warning("pypdf is not loaded")
        return False
    _, extn = os.path.splitext(sourcefile)
    if extn.lower() != '.pdf':
        logger.warning(f"Cannot swap cover on [{sourcefile}]")
        return False

    try:
        writer = PdfWriter()
        with open(sourcefile, "rb") as f:
            reader = PdfReader(f, strict=True)
            cnt = reader.get_num_pages()
            logger.debug(f"{sourcefile} has {cnt} pages")
            p = 0
            while p < cnt:
                writer.add_page(reader.pages[p])
                p += 1
        return True
    except Exception as e:
        logger.warning(str(e))
        return False


def coverswap(sourcefile, coverpage=2):
    logger = logging.getLogger(__name__)
    if PdfWriter is None:
        logger.warning("pypdf is not loaded")
        return False

    _, extn = os.path.splitext(sourcefile)
    if extn.lower() != '.pdf':
        logger.warning(f"Cannot swap cover on [{sourcefile}]")
        return False
    try:
        # reordering pages is quite slow if the source is on a networked drive
        # so work on a local copy, then move it over.
        original = sourcefile
        logger.debug(f"Copying {original}")
        srcfile = safe_copy(original, os.path.join(DIRS.CACHEDIR, os.path.basename(sourcefile)))
        writer = PdfWriter()
        with open(srcfile, "rb") as f:
            reader = PdfReader(f)
            cnt = reader.get_num_pages()
            logger.debug(f"{srcfile} has {cnt} pages, new cover from page {coverpage}")
            coverpage -= 1  # zero based page count
            writer.add_page(reader.pages[coverpage])
            p = 0
            while p < cnt:
                if p != coverpage:
                    writer.add_page(reader.pages[p])
                p += 1
            with open(f"{srcfile}new", "wb") as outputStream:
                writer.write(outputStream)
        logger.debug("Writing new output file")
        try:
            newcopy = safe_copy(f"{srcfile}new", f"{original}new")
        except Exception as e:
            logger.warning(f"Failed to copy output file: {str(e)}")
            return False
        os.remove(srcfile)
        os.remove(f"{srcfile}new")
        # windows does not allow rename to overwrite an existing file
        os.remove(original)
        os.rename(newcopy, original)
        logger.info(f"{sourcefile} has {cnt:d} pages. Swapped pages 1 and 2\n")
        return True

    except Exception as e:
        logger.warning(str(e))
        return False


def get_author_images():
    """ Try to get an author image for all authors without one"""
    logger = logging.getLogger(__name__)
    db = database.DBConnection()
    try:
        cmd = ("select AuthorID, AuthorName from authors where (instr(AuthorImg, 'nophoto') > 0 or "
               "AuthorImg is null) and Manual is not '1'")
        authors = db.select(cmd)
        if authors:
            logger.info(f'Checking images for {len(authors)} {plural(len(authors), "author")}')
            counter = 0
            for author in authors:
                authorid = author['AuthorID']
                imagelink = get_author_image(authorid)
                new_value_dict = {}
                if not imagelink:
                    logger.debug(f"No image found for {author['AuthorName']}")
                    new_value_dict = {"AuthorImg": 'images/nophoto.png'}
                elif 'nophoto' not in imagelink:
                    logger.debug(f"Updating {author['AuthorName']} image to {imagelink}")
                    new_value_dict = {"AuthorImg": imagelink}

                if new_value_dict:
                    counter += 1
                    control_value_dict = {"AuthorID": authorid}
                    db.upsert("authors", new_value_dict, control_value_dict)

            msg = f"Updated {counter} {plural(counter, 'image')}"
            logger.info(f"Author Image check complete: {msg}")
        else:
            msg = 'No missing author images'
            logger.debug(msg)
    finally:
        db.close()
    return msg


def get_book_covers():
    """ Try to get a cover image for all books """

    logger = logging.getLogger(__name__)
    db = database.DBConnection()
    try:
        cmd = ("select BookID,BookImg from books where instr(BookImg,'nocover') > 0 "
               "or instr(BookImg, 'nophoto') > 0 and Manual is not '1'")
        books = db.select(cmd)
        if books:
            logger.info(f"Checking covers for {len(books)} {plural(len(books), 'book')}")
            counter = 0
            for book in books:
                bookid = book['BookID']
                coverlink, _ = get_book_cover(bookid)
                if coverlink and "nocover" not in coverlink and "nophoto" not in coverlink:
                    control_value_dict = {"BookID": bookid}
                    new_value_dict = {"BookImg": coverlink}
                    db.upsert("books", new_value_dict, control_value_dict)
                    counter += 1
                if not coverlink and "http" in book['BookImg']:
                    control_value_dict = {"BookID": bookid}
                    new_value_dict = {"BookImg": "images/nocover.png"}
                    db.upsert("books", new_value_dict, control_value_dict)
            msg = f"Updated {counter} {plural(counter, 'cover')}"
            logger.info(f"Cover check complete: {msg}")
        else:
            msg = 'No missing book covers'
            logger.debug(msg)
    finally:
        db.close()
    return msg


def cache_bookimg(img, bookid, src, suffix='', imgid=None):
    logger = logging.getLogger(__name__)
    if not imgid:
        imgid = bookid
    if src:
        coverlink, success, _ = cache_img(ImageType.BOOK, imgid + suffix, img)
    else:
        coverlink, success, _ = cache_img(ImageType.BOOK, imgid, img, refresh=True)
        src = suffix

    # if librarything has no image they return a 1x1 gif
    data = ''
    coverfile = os.path.join(DIRS.DATADIR, coverlink)
    if path_isfile(coverfile):
        with open(syspath(coverfile), 'rb') as f:
            data = f.read()
    if len(data) < 50:
        logger.debug(f"Got an empty {src} image for {bookid} [{img}]")
    elif success:
        logger.debug(f"Caching {src} cover for {bookid}")
        return coverlink
    else:
        logger.debug(f"Failed to cache {src} image for {img} [{coverlink}]")
    return ''


def get_book_cover(bookid=None, src=None, ignore=''):
    """ Return link to a local file containing a book cover image for a bookid, and which source used.
        Try 1. Local file cached from goodreads/googlebooks when book was imported
            2. cover.jpg if we have the book
            3. LibraryThing cover image (if you have a dev key)
            4. LibraryThing whatwork (if available)
            5. Goodreads search (if book was imported from goodreads)
            6. OpenLibrary image
            7. Google isbn search (if google has a link to book for sale)
            8. Google images search (if lazylibrarian config allows)

        src = cache, cover, goodreads, librarything, whatwork, googleisbn, openlibrary, googleimage
        ignore = list of sources to skip
        Return None if no cover available. """
    logger = logging.getLogger(__name__)
    if not bookid:
        logger.error("get_book_cover- No bookID")
        return None, src

    if not src:
        src = ''
        imgid = img_id()
    else:
        imgid = None

    logger.debug(f"Getting {src} cover for {bookid}, ignore [{ignore}]")
    db = database.DBConnection()
    # noinspection PyBroadException
    try:
        cachedir = DIRS.CACHEDIR
        item = db.match('select BookImg from books where bookID=?', (bookid,))
        if item and item['BookImg']:
            coverlink = item['BookImg']
            coverfile = os.path.join(cachedir, coverlink.replace('cache/', ''))
            if coverlink != 'images/nocover.png' and 'nocover' in coverlink or 'nophoto' in coverlink:
                coverfile = os.path.join(DIRS.DATADIR, 'images', 'nocover.png')
                coverlink = 'images/nocover.png'
                db.action("UPDATE books SET BookImg=? WHERE BookID=?", (coverlink, bookid))
        else:
            coverlink = f"cache/book/{bookid}.jpg"
            coverfile = os.path.join(cachedir, "book", f"{bookid}.jpg")
        if not src or src == 'cache' or src == 'current':
            if path_isfile(coverfile):  # use cached image if there is one
                lazylibrarian.CACHE_HIT = int(lazylibrarian.CACHE_HIT) + 1
                return coverlink, 'cache'
            elif src:
                lazylibrarian.CACHE_MISS = int(lazylibrarian.CACHE_MISS) + 1
                return None, src

        if not src or src == 'cover' and 'cover' not in ignore:
            item = db.match('select BookFile,Audiofile from books where bookID=?', (bookid,))
            if item:
                # get either ebook or audiobook if they exist
                bookfile = item['BookFile'] or item['AudioFile']
                if bookfile and path_isfile(bookfile):  # we may have a cover.jpg in the same folder
                    bookdir = os.path.dirname(bookfile)
                    coverimg = jpg_file(bookdir)
                    if coverimg:
                        if src:
                            extn = '_cover.jpg'
                        else:
                            extn = '.jpg'

                        coverfile = os.path.join(cachedir, "book", bookid + extn)
                        coverlink = f"cache/book/{bookid}{extn}"
                        logger.debug(f"Caching {extn} for {bookid}")
                        _ = safe_copy(coverimg, coverfile)
                        return coverlink, src
                    else:
                        logger.debug(f"No cover found for {bookid} in {bookdir}")
                else:
                    if bookfile:
                        logger.debug(f"File {bookfile} not found")
            else:
                logger.debug(f"BookID {bookid} not found")
            if src:
                return None, src

        # see if librarything has a cover
        if not src or src == 'librarything' and 'librarything' not in ignore:
            if CONFIG['LT_DEVKEY']:
                cmd = "select BookISBN from books where bookID=?"
                item = db.match(cmd, (bookid,))
                if item and item['BookISBN']:
                    img = '/'.join([CONFIG['LT_URL'], f"devkey/{CONFIG['LT_DEVKEY']}/large/isbn/{item['BookISBN']}"])
                    coverlink = cache_bookimg(img, bookid, src, suffix='_lt', imgid=imgid)
                    if coverlink:
                        return coverlink, 'librarything'
                else:
                    logger.debug(f"No isbn for {bookid}")
            if src:
                return None, src

        # see if librarything workpage has a cover
        if NEW_WHATWORK and (not src or src == 'whatwork' and 'whatwork' not in ignore):
            work = get_bookwork(bookid, "Cover")
            if work and 'whatwork' not in ignore:
                try:
                    img = work.split('workCoverImage')[1].split('="')[1].split('"')[0]
                    if img:
                        coverlink = cache_bookimg(img, bookid, src, suffix='_ww', imgid=imgid)
                        if coverlink:
                            return coverlink, 'whatwork'
                    else:
                        logger.debug(f"No image found in work page for {bookid}")
                except IndexError:
                    logger.debug(f"workCoverImage not found in work page for {bookid}")

                try:
                    img = work.split('og:image')[1].split('="')[1].split('"')[0]
                    if img:
                        coverlink = cache_bookimg(img, bookid, src, suffix='_ww', imgid=imgid)
                        if coverlink:
                            return coverlink, 'whatwork'
                    else:
                        logger.debug(f"No image found in work page for {bookid}")
                except IndexError:
                    logger.debug(f"og:image not found in work page for {bookid}")
            else:
                logger.debug(f"No work page for {bookid}")
            if src:
                return None, src

        cmd = ("select BookName,AuthorName,BookLink,BookISBN,books.gr_id,books.hc_id,books.ol_id"
               " from books,authors where bookID=? and books.AuthorID = authors.AuthorID")
        item = db.match(cmd, (bookid,))
        if not item:
            return None, src

        title = safe_unicode(item['BookName'])
        author = safe_unicode(item['AuthorName'])
        safeparams = quote_plus(make_utf8bytes(f"{author} {title}")[0])

        # see if hardcover has a cover
        if not src or src == 'hardcover' and 'hardcover' not in ignore:
            if item['hc_id']:
                h_c = lazylibrarian.hc.HardCover(item['hc_id'])
                bookdict, _ = h_c.get_bookdict(item['hc_id'])
                img = bookdict.get('cover')
                if img:
                    coverlink = cache_bookimg(img, bookid, src, suffix='_hc', imgid=imgid)
                    if coverlink:
                        return coverlink, 'hardcover'
            if src:
                return None, src

        # try to get a cover from goodreads
        if not src or src == 'goodreads' and 'goodreads' not in ignore:
            if item['gr_id']:
                # if we have a goodreads bookid, we can call https://www.goodreads.com/book/show/{bookID}
                # and scrape the page for og:image
                # <meta property="og:image" content="https://i.gr-assets.com/images/S/photo.goodreads.com/books/
                # 1388267702i/16304._UY475_SS475_.jpg"/>
                # to get the cover
                booklink = '/'.join([CONFIG['GR_URL'], 'book', 'show', item['gr_id']])
                result, success = fetch_url(booklink)
                if success:
                    try:
                        img = result.split('id="coverImage"')[1].split('src="')[1].split('"')[0]
                    except IndexError:
                        try:
                            img = result.split('og:image')[1].split('="')[1].split('"')[0]
                        except IndexError:
                            img = None
                    if img and img.startswith('http') and 'nocover' not in img and 'nophoto' not in img:
                        coverlink = cache_bookimg(img, bookid, src, suffix='_gr', imgid=imgid)
                        if coverlink:
                            return coverlink, 'goodreads'
                    else:
                        logger.debug(f"No image found in goodreads page for {bookid}")
                else:
                    logger.debug(f"Error getting goodreads page {booklink}, [{result}]")
            if src:
                return None, src

        # try to get a cover from openlibrary
        if not src or src == 'openlibrary' and 'openlibrary' not in ignore:
            if item and item['ol_id']:
                baseurl = f"https://covers.openlibrary.org/b/olid/{item['ol_id']}-M.jpg"
                result, success = fetch_url(baseurl, raw=True)
                if success:
                    with tempfile.NamedTemporaryFile(delete_on_close=False) as f:
                        f.write(result)
                        f.close()
                        coverlink = cache_bookimg(f.name, bookid, src, suffix='_ol', imgid=imgid)
                        if coverlink:
                            return coverlink, 'openlibrary'

            if item and item['BookISBN']:
                baseurl = '/'.join([CONFIG['OL_URL'],
                                   'api/books?format=json&jscmd=data&bibkeys=ISBN:'])
                result, success = fetch_url(baseurl + item['BookISBN'])
                if success:
                    try:
                        source = json.loads(result)  # type: dict
                    except Exception as e:
                        logger.debug(f"OpenLibrary json error: {e}")
                        source = {}

                    img = ''
                    if source:
                        # noinspection PyUnresolvedReferences
                        k = list(source.keys())[0]
                        try:
                            img = source[k]['cover']['medium']
                        except KeyError:
                            try:
                                img = source[k]['cover']['large']
                            except KeyError:
                                logger.debug(f"No openlibrary image for {item['BookISBN']}")

                    if img and img.startswith('http') and 'nocover' not in img and 'nophoto' not in img:
                        coverlink = cache_bookimg(img, bookid, src, suffix='_ol', imgid=imgid)
                        if coverlink:
                            return coverlink, 'openlibrary'
                else:
                    logger.debug(f"OpenLibrary error: {result}")
                    BLOCKHANDLER.block_provider("openlibrary", result)
            if src:
                return None, src

        if not src or src == 'googleisbn' and 'googleapis' not in ignore:
            # try a google isbn page search...
            if item and item['BookISBN']:
                url = f"https://www.googleapis.com/books/v1/volumes?q=isbn:{item['BookISBN']}"
                result, success = fetch_url(url)
                if success:
                    img = ''
                    try:
                        source = json.loads(result)  # type: dict
                        img = source["items"][0]["volumeInfo"]["imageLinks"]["thumbnail"]
                    except (IndexError, KeyError):
                        pass
                    except Exception as e:
                        logger.debug(f"GoogleISBN {type(e).__name__}: {e}")

                    if img:
                        coverlink = cache_bookimg(img, bookid, src, suffix='_gi', imgid=imgid)
                        if coverlink:
                            return coverlink, 'googleisbn'
                    else:
                        logger.debug(f"No image found in google isbn page for {bookid}")
                else:
                    logger.debug("Failed to fetch url from google")
            else:
                logger.debug(f"No isbn to search for {bookid}")
            if src:
                return None, src

        if PIL and safeparams:
            if not src or src == 'baidu' and 'baidu' not in ignore:
                res, src = crawl_image('baidu', src, cachedir, bookid, safeparams, imgid=imgid)
                if res:
                    return res, src
            if not src or src == 'bing' and 'bing' not in ignore:
                res, src = crawl_image('bing', src, cachedir, bookid, safeparams, imgid=imgid)
                if res:
                    return res, src
            if not src or src == 'flikr' and 'flikr' not in ignore:
                res, src = crawl_image('flickr', src, cachedir, bookid, safeparams, imgid=imgid)
                if res:
                    return res, src
            if not src or src == 'googleimage' and 'googleapis' not in ignore:
                res, src = crawl_image('google', src, cachedir, bookid, safeparams, imgid=imgid)
                if res:
                    return res, src

        logger.debug("No image found from any configured source")
        return None, src
    except Exception:
        logger.error(f'Unhandled exception in get_book_cover: {traceback.format_exc()}')
    finally:
        db.close()
    return None, src


def crawl_image(crawler_name, src, cachedir, bookid, safeparams, imgid=None):
    """ Searches for images, finds at most one image.
      crawler_name: baidu, bing, flickr or google
      src: Used to name the file. Same as crawler, or googleimage
      cachedir: Files found will be stored in a subdir of this
      bookid: ID of the book
      safeparams: Search used in the search
    Returns
      None, src if the search is unsuccessful
      coverlink, crawler_name if successful
    """
    logger = logging.getLogger(__name__)
    # for key in logging.Logger.manager.loggerDict:
    #     print(key)
    logging.getLogger('lib.icrawler').setLevel(logging.CRITICAL)
    logging.getLogger('feeder').setLevel(logging.CRITICAL)
    logging.getLogger('downloader').setLevel(logging.CRITICAL)
    logging.getLogger('parser').setLevel(logging.CRITICAL)

    icrawlerdir = os.path.join(cachedir, 'icrawler', bookid)
    if crawler_name == 'baidu':
        crawler = BaiduImageCrawler(storage={'root_dir': icrawlerdir})
    elif crawler_name == 'bing':
        crawler = BingImageCrawler(storage={'root_dir': icrawlerdir})
    elif crawler_name == 'flickr':
        crawler = FlickrImageCrawler(storage={'root_dir': icrawlerdir})
    else:
        crawler_name = 'google'
        crawler = GoogleImageCrawler(storage={'root_dir': icrawlerdir})

    crawler.crawl(keyword=safeparams, max_num=1)
    if os.path.exists(icrawlerdir):
        res = len(os.listdir(icrawlerdir))
    else:
        res = 0
    logger.debug(f"{crawler_name} found {res} {plural(res, 'image')}")
    if res:
        img = os.path.join(icrawlerdir, os.listdir(icrawlerdir)[0])

        coverlink = cache_bookimg(img, bookid, src, suffix=f"_{crawler_name[:2]}", imgid=imgid)
        rmtree(icrawlerdir, ignore_errors=True)
        if coverlink:
            return coverlink, crawler_name
    else:
        logger.debug(f"No images found in {crawler_name} page for {bookid}")
    # rmtree(icrawlerdir, ignore_errors=True)
    return None, src


def get_author_image(authorid=None, refresh=False, max_num=1):
    logger = logging.getLogger(__name__)
    if not authorid:
        logger.error("get_author_image: No authorid")
        return None

    db = database.DBConnection()
    try:
        author = db.match('select AuthorName,AuthorIMG from authors where AuthorID=?', (authorid,))
    finally:
        db.close()

    cachedir = DIRS.CACHEDIR
    datadir = DIRS.DATADIR
    if author:
        coverfile = os.path.join(datadir, author['AuthorIMG'])
    else:
        coverfile = os.path.join(cachedir, "author", f"{authorid}.jpg")

    if path_isfile(coverfile) and max_num == 1 and not refresh:  # use cached image if there is one
        lazylibrarian.CACHE_HIT = int(lazylibrarian.CACHE_HIT) + 1
        logger.debug(f"get_author_image: Returning Cached response for {coverfile}")
        coverlink = coverfile.lstrip(datadir)
        return coverlink

    lazylibrarian.CACHE_MISS = int(lazylibrarian.CACHE_MISS) + 1
    if PIL and author:
        authorname = safe_unicode(author['AuthorName'])
        safeparams = quote_plus(make_utf8bytes(f"author {authorname}")[0])
        icrawlerdir = os.path.join(cachedir, 'icrawler', authorid)
        rmtree(icrawlerdir, ignore_errors=True)
        crawler_name = 'google'
        gc = GoogleImageCrawler(storage={'root_dir': icrawlerdir})
        gc.crawl(keyword=safeparams, max_num=int(max_num))
        if os.path.exists(icrawlerdir):
            res = len(os.listdir(icrawlerdir))
        else:
            # nothing from google, try bing
            crawler_name = 'bing'
            bc = BingImageCrawler(storage={'root_dir': icrawlerdir})
            bc.crawl(keyword=safeparams, max_num=int(max_num))
            if os.path.exists(icrawlerdir):
                res = len(os.listdir(icrawlerdir))
            else:
                res = 0
        logger.debug(f"{crawler_name} found {res} {plural(res, 'image')}")
        if max_num == 1:
            if res:
                img = os.path.join(icrawlerdir, os.listdir(icrawlerdir)[0])
                coverlink, success, _ = cache_img(ImageType.AUTHOR, img_id(), img, refresh=refresh)
                if success:
                    logger.debug(f"Cached {crawler_name} image for {authorname}")
                    return coverlink
            else:
                logger.debug(f"No images found for {authorname}")
            rmtree(icrawlerdir, ignore_errors=True)
        else:
            return icrawlerdir
    elif not PIL:
        logger.debug("PIL not installed, not looking for author image")
    else:
        logger.debug(f"No author found for {authorid}")
    return None


def create_mag_covers(refresh=False):
    logger = logging.getLogger(__name__)
    if not CONFIG.get_bool('IMP_MAGCOVER'):
        logger.info('Cover creation is disabled in config')
        return ''
    db = database.DBConnection()
    try:
        #  <> '' ignores empty string or NULL
        issues = db.select("SELECT Title,IssueFile from issues WHERE IssueFile <> ''")
        if refresh:
            logger.info(f"Creating covers for {len(issues)} {plural(len(issues), 'issue')}")
        else:
            logger.info(f"Checking covers for {len(issues)} {plural(len(issues), 'issue')}")
        cnt = 0
        for item in issues:
            try:
                maginfo = db.match("SELECT CoverPage from magazines WHERE Title=?", (item['Title'],))
                create_mag_cover(item['IssueFile'], refresh=refresh, pagenum=maginfo['CoverPage'])
                cnt += 1
            except Exception as why:
                logger.warning(f"Unable to create cover for {item['IssueFile']}, {type(why).__name__} {str(why)}")
    finally:
        db.close()
    logger.info("Cover creation completed")
    if refresh:
        return f"Created covers for {cnt} {plural(cnt, 'issue')}"
    return f"Checked covers for {cnt} {plural(cnt, 'issue')}"


def find_gs():
    global GS, GS_VER, generator
    logger = logging.getLogger(__name__)
    if not GS:
        if os.name == 'nt':
            GS = os.path.join(os.getcwd(), "gswin64c.exe")
            generator = "local gswin64c"
            if not path_isfile(GS):
                logger.debug(f"{GS} not found")
                GS = os.path.join(os.getcwd(), "gswin32c.exe")
                generator = "local gswin32c"
            if not path_isfile(GS):
                logger.debug(f"{GS} not found")
                params = ["where", "gswin64c"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = make_unicode(GS).strip()
                    generator = "gswin64c"
                except Exception as e:
                    logger.debug(f"where gswin64c failed: {type(e).__name__} {str(e)}")
                    GS = ''
            if not path_isfile(GS):
                params = ["where", "gswin32c"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = make_unicode(GS).strip()
                    generator = "gswin32c"
                except Exception as e:
                    logger.debug(f"where gswin32c failed: {type(e).__name__} {str(e)}")
            if not path_isfile(GS):
                logger.debug("No gswin found")
                generator = "(no windows ghostscript found)"
                GS = ''
        else:
            GS = os.path.join(os.getcwd(), "gs")
            generator = "local gs"
            if not path_isfile(GS):
                logger.debug(f"{GS} not found")
                GS = ''
                params = ["which", "gs"]
                try:
                    GS = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    GS = make_unicode(GS).strip()
                    generator = GS
                except subprocess.CalledProcessError as e:
                    if e.returncode == 1:
                        logger.debug("Could not find gs in your system path")
                    else:
                        logger.debug(f"which gs failed: {type(e).__name__} {str(e)}")
            if not path_isfile(GS):
                logger.debug("Cannot find gs")
                generator = "(no gs found)"
                GS = ''
        if GS:
            GS_VER = ''
            # noinspection PyBroadException
            try:
                params = [GS, "--version"]
                res = subprocess.check_output(params, stderr=subprocess.STDOUT)
                res = make_unicode(res).strip()
                logger.debug(f"Found {generator} [{GS}] version {res}")
                generator = f"{generator} version {res}"
                GS_VER = res
            except Exception as e:
                logger.debug(f"gs --version failed: {type(e).__name__} {str(e)}")

    return GS, GS_VER, generator


def shrink_mag(issuefile, dpi=0):
    global GS, GS_VER, generator
    logger = logging.getLogger(__name__)
    if not issuefile or not path_isfile(issuefile):
        logger.warning(f'No issuefile {issuefile}')
        return ''
    if not GS:
        GS, GS_VER, generator = find_gs()
    if GS_VER:
        outfile = f"{issuefile}_{dpi}.pdf"
        params = [GS, "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                  "-dCompatibilityLevel=1.3", "-dPDFSETTINGS=/screen",
                  "-dEmbedAllFonts=true", "-dSubsetFonts=true",
                  "-dAutoRotatePages=/None",
                  "-dColorImageDownsampleType=/Bicubic",
                  f"-dColorImageResolution={dpi}",
                  "-dGrayImageDownsampleType=/Bicubic",
                  f"-dGrayImageResolution={dpi}",
                  "-dMonoImageDownsampleType=/Subsample",
                  f"-dMonoImageResolution={dpi}",
                  "-dUseCropBox", f"-sOutputFile={outfile}", issuefile]
        try:
            res = subprocess.check_output(params, stderr=subprocess.STDOUT)
            res = make_unicode(res).strip()
            if not path_isfile(outfile):
                logger.debug(f"Failed to shrink file: {res}")
                return ''
            logger.debug(f"Resized file: {outfile}")
            return outfile
        except Exception as e:
            logger.debug(f"Failed to shrink file with {str(params)} [{e}]")
            return ''


# noinspection PyUnresolvedReferences
def create_mag_cover(issuefile=None, refresh=False, pagenum=1):
    global GS, GS_VER, generator
    logger = logging.getLogger(__name__)
    if not CONFIG.get_bool('IMP_MAGCOVER') or not pagenum:
        logger.warning(f'No cover required for {issuefile}')
        return ''
    if not issuefile or not path_isfile(issuefile):
        logger.warning(f'No issuefile {issuefile}')
        return ''

    base, extn = os.path.splitext(issuefile)
    if not extn:
        logger.warning(f'Unable to create cover for {issuefile}, no extension?')
        return ''

    coverfile = f"{base}.jpg"

    if path_isfile(coverfile):
        if refresh:
            os.remove(syspath(coverfile))
        else:
            logger.debug(f'Cover for {issuefile} exists')
            return coverfile  # quit if cover already exists and we didn't want to refresh

    logger.debug(f'Creating cover for {issuefile}, page {pagenum}')
    data = ''  # result from unzip or unrar
    extn = extn.lower()
    if extn in ['.cbz', '.epub']:
        try:
            data = zipfile.ZipFile(issuefile)
        except Exception as why:
            logger.error(f"Failed to read zip file {issuefile}, {type(why).__name__} {str(why)}")
            data = ''
    elif extn in ['.cbr']:
        if lazylibrarian.UNRARLIB:
            try:
                if lazylibrarian.UNRARLIB == 1:
                    data = lazylibrarian.RARFILE.RarFile(issuefile)
                elif lazylibrarian.UNRARLIB == 2:
                    data = lazylibrarian.RARFILE(issuefile)
            except Exception as why:
                logger.error(f"Failed to read rar file {issuefile}, {type(why).__name__} {str(why)}")
                data = ''
    if data:
        img = None
        fextn = ''
        try:
            for item in ['cover.', '000.', '001.', '00.', '01.']:
                if getattr(data, 'infoiter', None):
                    for member in data.infoiter():
                        fname = member.filename.lower()
                        if item in fname:
                            _, fextn = os.path.splitext(fname)
                            if fextn in ['.jpg', '.jpeg', '.png', '.webp']:
                                r = data.read_files(member.filename)
                                img = r[0][1]
                                break
                else:
                    for member in data.namelist():
                        fname = member.lower()
                        if item in fname:
                            _, fextn = os.path.splitext(fname)
                            if fextn in ['.jpg', '.jpeg', '.png', '.webp']:
                                img = data.read(member)
                                break
                if img:
                    break
            if img:
                if PIL and fextn in ['.png', '.webp']:
                    image = PILImage.open(io.BytesIO(img))
                    image = image.convert('RGB')
                    image.save(syspath(coverfile), 'jpeg')
                else:
                    with open(syspath(coverfile), 'wb') as f:
                        f.write(img)
                return coverfile
            else:
                logger.debug(f"Failed to find image in {issuefile}")
        except Exception as why:
            logger.error(f"Failed to extract image from {issuefile}, {type(why).__name__} {str(why)}")

    elif extn == '.pdf':
        if len(CONFIG['IMP_CONVERT']):  # allow external convert to override libraries
            generator = f"external program: {CONFIG['IMP_CONVERT']}"
            if "gsconvert.py" in CONFIG['IMP_CONVERT']:
                msg = "Use of gsconvert.py is deprecated, equivalent functionality is now built in. "
                msg += "Support for gsconvert.py may be removed in a future release. See wiki for details."
                logger.warning(msg)
            converter = CONFIG['IMP_CONVERT']
            postfix = ''
            # if not path_isfile(converter):  # full path given, or just program_name?
            #     converter = os.path.join(os.getcwd(), lazylibrarian.CONFIG['IMP_CONVERT'])
            if 'convert' in converter and 'gs' not in converter:
                # tell imagemagick to only convert first page
                postfix = '[0]'
            params = []
            try:
                params = [converter, f'{issuefile}{postfix}', f'{coverfile}']
                if os.name != 'nt':
                    res = subprocess.check_output(params, preexec_fn=lambda: os.nice(10),
                                                  stderr=subprocess.STDOUT)
                else:
                    res = subprocess.check_output(params, stderr=subprocess.STDOUT)

                res = make_unicode(res).strip()
                if res:
                    logger.debug(f"{CONFIG['IMP_CONVERT']} reports: {res}")
            except Exception as e:
                if params:
                    logger.debug(params)
                logger.warning(f'External "convert" failed {type(e).__name__} {str(e)}')

        elif os.name == 'nt':
            if not GS:
                GS, GS_VER, generator = find_gs()
            if GS_VER:
                issuefile = issuefile.split('[')[0]
                params = [GS, "-sDEVICE=jpeg", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                          f"-dFirstPage={check_int(pagenum, 1):d}",
                          f"-dLastPage={check_int(pagenum, 1):d}",
                          "-dUseCropBox", f"-sOutputFile={coverfile}", issuefile]
                try:
                    res = subprocess.check_output(params, stderr=subprocess.STDOUT)
                    res = make_unicode(res).strip()
                    if not path_isfile(coverfile):
                        logger.debug(f"Failed to create jpg: {res}")
                except Exception as e:
                    logger.debug(f"Failed to create cover with {str(params)} [{e}]")
            else:
                logger.warning(f"Failed to create jpg for {issuefile}")
        else:  # not windows
            try:
                # noinspection PyUnresolvedReferences,PyPep8Naming
                from wand.image import Image as wand_image
                interface = "wand"
            except ImportError:
                wand_image = None
                try:
                    # No PythonMagick in python3
                    # noinspection PyUnresolvedReferences,PyPep8Naming
                    from PythonMagick import Image as pythonmagick_image
                    interface = "pythonmagick"
                except ImportError:
                    interface = ""
            try:
                if interface == 'wand':
                    generator = "wand interface"
                    with wand_image(filename=f"{issuefile}[{str(check_int(pagenum, 1) - 1)}]") as img:
                        img.save(filename=coverfile)

                elif interface == 'pythonmagick':
                    generator = "pythonmagick interface"
                    img = pythonmagick_image()
                    # PythonMagick requires filenames to be bytestr, not unicode
                    if type(issuefile) is str:
                        issuefile = make_bytestr(issuefile)
                    if type(coverfile) is str:
                        coverfile = make_bytestr(coverfile)
                    img.read(f"{issuefile}[{str(check_int(pagenum, 1) - 1)}]")
                    img.write(coverfile)

                else:
                    if not GS:
                        GS, GS_VER, generator = find_gs()
                    if GS_VER:
                        issuefile = issuefile.split('[')[0]
                        params = [GS, "-sDEVICE=jpeg", "-dNOPAUSE", "-dBATCH", "-dSAFER",
                                  f"-dFirstPage={check_int(pagenum, 1):d}",
                                  f"-dLastPage={check_int(pagenum, 1):d}",
                                  "-dUseCropBox", f"-sOutputFile={coverfile}", issuefile]
                        try:
                            res = subprocess.check_output(params, preexec_fn=lambda: os.nice(10),
                                                          stderr=subprocess.STDOUT)
                            res = make_unicode(res).strip()
                            if not path_isfile(coverfile):
                                logger.debug(f"Failed to create jpg: {res}")
                        except Exception as e:
                            logger.debug(f"Failed to create cover with {str(params)} [{e}]")
                    else:
                        logger.warning(f"Failed to create jpg for {issuefile}")
            except Exception as e:
                logger.warning(f"Unable to create cover for {issuefile} using {type(e).__name__} {generator}")
                logger.debug(f"Exception in create_cover: {traceback.format_exc()}")

        if path_isfile(coverfile):
            setperm(coverfile)
            logger.debug(f"Created cover (page {check_int(pagenum, 1):d}) for {issuefile} using {generator}")
            return coverfile

    # if not recognised extension or cover creation failed
    try:
        coverfile = safe_copy(os.path.join(DIRS.PROG_DIR, 'data', 'images', 'nocover.jpg'), coverfile)
        setperm(coverfile)
        return coverfile
    except Exception as why:
        logger.error(f"Failed to copy nocover file, {type(why).__name__} {str(why)}")
    return ''
